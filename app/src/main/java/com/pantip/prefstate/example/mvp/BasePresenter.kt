package com.pantip.prefstate.example.mvp

/**
 * @author peerapongsam
 */
abstract class BasePresenter<VM, V : BaseContract.View<VM>> : BaseContract.Presenter<VM, V> {

    var view: V? = null

    var viewModel: VM = defaultViewModel()
        set(value) {
            field = value
            view?.onViewModelChanged(value)
        }

    abstract fun defaultViewModel(): VM

    override val currentViewModel: VM
        get() = viewModel

    override fun attachView(view: V) {
        this.view = view
        view.onViewModelChanged(viewModel)
    }

    override fun detachView() {
        this.view = null
    }

    override fun restoreViewModel(viewModel: VM) {
        this.viewModel = viewModel
    }
}