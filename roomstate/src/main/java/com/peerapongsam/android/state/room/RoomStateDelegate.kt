package com.peerapongsam.android.state.room

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.os.Parcel
import android.util.Base64
import com.pantip.android.realmstate.wrapper.WrapperUtils
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

internal class RoomStateDelegate
internal constructor(context: Context) {

    init {
        registerForLifecycleEvents(context)
    }

    companion object {
        private const val KEY_BUNDLE = "BUNDLE_%s"
        private const val KEY_UUID = "UUID_%s"
        private const val AUTO_DATA_CLEAR_INTERVAL_MS = 100
    }

    private val DB_NAME = RoomStateDelegate::class.java.name

    private var isClearAllowed: Boolean = false
    private var isFirstRestoreCall: Boolean = true
    private var lastClearTime: Long = 0
    private var uuidBundleMap: MutableMap<String, Bundle> = mutableMapOf()
    private var objectUuidMap: MutableMap<Any, String> = mutableMapOf()
    private var recentUuids: MutableSet<String> = mutableSetOf()
    private val database by lazy {
        BundleDatabase.database(context, DB_NAME)
    }

    private fun registerForLifecycleEvents(context: Context) {
        (context.applicationContext as Application).registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacksAdapter() {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                isClearAllowed = true
            }

            override fun onActivityDestroyed(activity: Activity) {
                isClearAllowed = !activity.isChangingConfigurations
            }
        })
    }

    fun clear(target: Any) {
        if (!isClearAllowed) return

        val uuid = objectUuidMap.remove(target) ?: return
        clearDataForUuid(uuid)
    }

    fun clearAll() {
        recentUuids.clear()
        uuidBundleMap.clear()
        objectUuidMap.clear()

        Flowable.fromCallable { database.bundleDao().deleteAllEntities() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ }, { })
    }

    private fun clearDataForUuid(uuid: String) {
        Flowable.fromCallable { database.bundleDao().deleteEntityById(uuid) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ }, { })
    }

    private fun clearStateData() {
        val currentTime = System.currentTimeMillis()
        if (currentTime - lastClearTime < AUTO_DATA_CLEAR_INTERVAL_MS) {
            return
        }
        lastClearTime = currentTime
        System.gc()

        val stateUuids = recentUuids.toHashSet()
        stateUuids.removeAll(objectUuidMap.values)
        stateUuids.forEach { clearDataForUuid(it) }
    }

    private fun getKeyForEncodedBundle(uuid: String): String {
        return String.format(Locale.US, KEY_BUNDLE, uuid)
    }

    private fun getKeyForUuid(target: Any): String {
        return String.format(Locale.US, KEY_UUID, target.javaClass.name)
    }

    private fun readFromDisk(uuid: String): Bundle? {
        val entity = database.bundleDao().getEntityById(uuid)
        if (entity?.value == null) {
            val parcelBytes = Base64.decode(entity!!.value, Base64.DEFAULT)
            val parcel = Parcel.obtain()
            parcel.unmarshall(parcelBytes, 0, parcelBytes.size)
            parcel.setDataPosition(0)
            val bundle = parcel.readBundle(RoomStateDelegate::class.java.classLoader)
            parcel.recycle()
            return bundle
        }
        return Bundle()
    }

    private fun writeToDisk(uuid: String, bundle: Bundle) {
        val parcel = Parcel.obtain()
        parcel.writeBundle(bundle)
        try {
            val parcelString = Base64.encodeToString(parcel.marshall(), Base64.DEFAULT)
            Flowable.fromCallable { database.bundleDao().insert(BundleEntity(uuid, parcelString)) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ }, { })
        } catch (e: Throwable) {
            // ignore
        } finally {
            parcel.recycle()
        }
    }

    fun saveInstanceState(target: Any, savedState: Bundle, superState: Bundle) {
        var uuid = objectUuidMap[target]
        if (uuid == null) {
            uuid = UUID.randomUUID().toString()
            objectUuidMap[target] = uuid
        }

        superState.putString(getKeyForUuid(target), uuid)
        WrapperUtils.wrapOptimizedObjects(savedState)
        recentUuids.add(uuid)
        uuidBundleMap[uuid] = savedState
        writeToDisk(uuid, savedState)
        clearStateData()
    }

    fun restoreInstanceState(target: Any, superState: Bundle?): Bundle? {
        val firstRestoreCall = isFirstRestoreCall
        isFirstRestoreCall = false
        if (superState == null) {
            if (firstRestoreCall) {

            }
            return null
        }

        val uuid: String = (if (objectUuidMap.containsKey(target))
            objectUuidMap[target]
        else
            superState.getString(getKeyForUuid(target), null)) ?: return null

        objectUuidMap[target] = uuid

        val savedState: Bundle = (if (uuidBundleMap.containsKey(uuid))
            uuidBundleMap[uuid]
        else
            readFromDisk(uuid)) ?: return null

        WrapperUtils.unwrapOptimizedObjects(savedState)
        clearDataForUuid(uuid)
        return savedState
    }
}