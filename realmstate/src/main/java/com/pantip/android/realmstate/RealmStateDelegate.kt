package com.pantip.android.realmstate

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.os.Parcel
import android.util.Base64
import com.pantip.android.realmstate.wrapper.WrapperUtils
import io.realm.Realm
import io.realm.RealmConfiguration
import java.util.*

/**
 * @author peerapongsam
 */
internal class RealmStateDelegate internal constructor(context: Context) {

    init {
        registerForLifecycleEvents(context)
    }

    companion object {
        private val KEY_BUNDLE = "bundle_%s"
        private val KEY_UUID = "uuid_%s"
        private val AUTO_DATA_CLEAR_INTERVAL_MS = 100
    }

    private val DB_NAME = RealmStateDelegate::class.java.name

    private var isClearAllowed: Boolean = false
    private var isFirstRestoreCall: Boolean = true
    private var lastClearTime: Long = 0
    private var uuidBundleMap: MutableMap<String, Bundle> = mutableMapOf()
    private var objectUuidMap: MutableMap<Any, String> = mutableMapOf()
    private var recentUuids: MutableSet<String> = mutableSetOf()
    private val realmConfig = RealmConfiguration.Builder()
            .name(DB_NAME + ".realm")
            .deleteRealmIfMigrationNeeded()
            .modules(RealmStateModule())
            .build()

    private fun registerForLifecycleEvents(context: Context) {
        (context.applicationContext as Application).registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacksAdapter() {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                isClearAllowed = true
            }

            override fun onActivityDestroyed(activity: Activity) {
                // Don't allow clearing during known configuration changes
                isClearAllowed = !activity.isChangingConfigurations
            }
        }
        )
    }

    fun clear(target: Any) {
        if (!isClearAllowed) return

        val uuid = objectUuidMap.remove(target) ?: return
        clearDataForUuid(uuid)
    }

    fun clearAll() {
        recentUuids.clear()
        uuidBundleMap.clear()
        objectUuidMap.clear()

        Realm.getInstance(realmConfig).use {
            it.executeTransaction { realm ->
                realm.deleteAll()
            }
        }
        Realm.deleteRealm(realmConfig)
    }

    private fun clearDataForUuid(uuid: String) {
        Realm.getInstance(realmConfig).use {
            it.executeTransaction { realm ->
                realm
                        .where(RealmBundle::class.java)
                        .equalTo("id", getKeyForEncodedBundle(uuid))
                        .findAll()
                        .deleteAllFromRealm()
            }
        }
    }

    private fun clearStateData() {
        val currentTime = System.currentTimeMillis()
        if (currentTime - lastClearTime < AUTO_DATA_CLEAR_INTERVAL_MS) {
            return
        }
        lastClearTime = currentTime
        System.gc()

        val staleUuids = recentUuids.toHashSet()
        staleUuids.removeAll(objectUuidMap.values)
        staleUuids.forEach { clearDataForUuid(it) }
    }

    private fun getKeyForEncodedBundle(uuid: String): String {
        return String.format(Locale.US, KEY_BUNDLE, uuid)
    }

    private fun getKeyForUuid(target: Any): String {
        return String.format(Locale.US, KEY_UUID, target.javaClass.name)
    }

    private fun readFromDisk(uuid: String): Bundle? {
        return Realm.getInstance(realmConfig).use {
            val realmStateString = it
                    .where(RealmBundle::class.java)
                    .equalTo("id", getKeyForEncodedBundle(uuid))
                    .findFirst()
            if (realmStateString?.value != null) {
                val parcelBytes = Base64.decode(realmStateString?.value, 0)
                val parcel = Parcel.obtain()
                parcel.unmarshall(parcelBytes, 0, parcelBytes.size)
                parcel.setDataPosition(0)
                val bundle = parcel.readBundle(RealmStateDelegate::class.java.classLoader)
                parcel.recycle()
                it.close()
                bundle
            } else {
                return Bundle()
            }
        }
    }

    private fun writeToDisk(uuid: String, bundle: Bundle) {
        val parcel = Parcel.obtain()
        parcel.writeBundle(bundle)
        try {
            val encodedString = Base64.encodeToString(parcel.marshall(), 0)
            Realm.getInstance(realmConfig).use {
                it.executeTransaction { realm ->
                    realm.where(RealmBundle::class.java)
                    val realmBundle = RealmBundle(getKeyForEncodedBundle(uuid), encodedString)
                    realm.copyToRealmOrUpdate(realmBundle)
                }
            }
            parcel.recycle()
        } catch (t: Throwable) {
            //ignore
        }
    }

    fun saveInstanceState(target: Any, savedState: Bundle, superState: Bundle) {
        var uuid = objectUuidMap[target]
        if (uuid == null) {
            uuid = UUID.randomUUID().toString()
            objectUuidMap.put(target, uuid)
        }
        superState.putString(getKeyForUuid(target), uuid)
        WrapperUtils.wrapOptimizedObjects(savedState)
        recentUuids.add(uuid)
        uuidBundleMap.put(uuid, savedState)
        writeToDisk(uuid, savedState)
        clearStateData()
    }

    fun restoreInstanceState(target: Any, superState: Bundle?): Bundle? {
        val firstRestore = isFirstRestoreCall
        isFirstRestoreCall = false
        if (superState == null) {
            if (firstRestore) {
                Realm.getInstance(realmConfig).use {
                    it.executeTransaction { realm ->
                        realm.deleteAll()
                    }
                }
                Realm.deleteRealm(realmConfig)
            }
            return null
        }
        val uuid: String = (if (objectUuidMap.containsKey(target))
            objectUuidMap[target]
        else
            superState.getString(getKeyForUuid(target), null)) ?: return null

        objectUuidMap.put(target, uuid)

        val savedState = (if (uuidBundleMap.containsKey(uuid))
            uuidBundleMap[uuid]
        else
            readFromDisk(uuid)) ?: return null

        WrapperUtils.unwrapOptimizedObjects(savedState)
        clearDataForUuid(uuid)
        return savedState
    }
}